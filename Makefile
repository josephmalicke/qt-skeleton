.PHONY: all
all:
	mkdir -p build
	cd build && qmake .. && make

.PHONY: clean
clean:
	rm -r build
