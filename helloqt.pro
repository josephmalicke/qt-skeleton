TARGET = helloqt
TEMPLATE = app
CONFIG += c++11 debug
QT += widgets

INCLUDEPATH += src

SOURCES += src/main.cpp
SOURCES += src/MainWindow.cpp

HEADERS += src/MainWindow.h

FORMS += src/res/forms/MainWindow.ui